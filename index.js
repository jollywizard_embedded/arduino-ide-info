const src = './src'

module.exports = require(`${src}/ArduinoIDE`)
module.exports.class = {
  ArduinoIDE        : module.exports
, ArduinoSketchbook : require(`${src}/ArduinoSketchbook`)
, ArduinoLibrary    : require(`${src}/ArduinoLibrary`)
}
