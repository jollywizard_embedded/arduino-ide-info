const path = require('@jamesarlow/tilde-path')
const _ = require('lodash')
const fs = require('fs')

const ArduinoSketchbook = require('./ArduinoSketchbook')
const ArduinoPreferences = require('./ArduinoPreferences')

class ArduinoIDE {

  static get default()
  {
    return new ArduinoIDE(ArduinoIDE.defaultOptions)
  }

  static get defaultOptions() {
    return {
      meta:findDefaultMetaDir()
    }
  }

  constructor(opts)
  {
    this._metaDir = opts.meta
  }

  /**
  The metaDir is the directory where the preferences file is stored.
  */
  get metaDir()
  {
    return this._metaDir
  }

  get preferences()
  {
    return new ArduinoPreferences({ide:this})
  }

  get sketchbook() {
    return new ArduinoSketchbook({ide:this})
  }
}

/**
  Gets the path of the meta-dir that holds the system
  configuration info.
*/
function findDefaultMetaDir() {

  const candidates = _.chain([
    '~/.arduino15/'
    // TODO: Add windows candidates
  ])

  .map(path.normalize)
  .value()

  const find = _.find(candidates, fs.existsSync)

  return !!find ? find : _.first(candidates)
}


let e = module.exports = ArduinoIDE
