const fs = require('fs')

const ArduinoPreferences = require('./ArduinoPreferences')

class ArduinoLibrary {

  constructor(opts)
  {
    this._path = opts.path
  }

  get path() {
    return this._path
  }

  get exists() {
    return this.properties.exists
  }

  get properties()
  {
    return new ArduinoLibraryProperties({library:this})
  }
}

class ArduinoLibraryProperties {

  static get FILENAME () { return 'library.properties' }

  /**
  This is the list of keys recognized by the ArudinoIDE for
  the library.preferences files.  The IDE expects each of these
  to be valid strings for a library to be installed.

  NOTE: Invalid key entries will create errors with the IDE, but it may or may
  not cause an issue with imports after hot installing to a running ide.
  */
  static get KEYS() {
    return [
      'name'
    , 'version'
    , 'author'
    , 'maintainer'
    , 'sentence'
    , 'paragraph'
    , 'url'
    , 'category'
    , 'url'
    , 'architectures'
    ]
  }

  constructor(opts)
  {
    this._library = opts.library
  }

  get library()
  {
    return this._library
  }

  get path()
  {
    return `${this.library.path}/`
         + ArduinoLibraryProperties.FILENAME
  }

  get realpath()
  {
    if (!this.exists)
      return undefined

    if (fs.lstat(this.path).isSymbolicLink())
      return fs.readlink(this.path)

    return this.path
  }

  get exists() {
    return fs.existsSync(this.path)
  }

  read() {
    return this.exists ? new PropertyReader(this.path) : undefined
  }

  getProperty(key) {
    return this.exists ? this.read().get(key) : undefined
  }
}

for (let key of ArduinoLibraryProperties.KEYS)
{
  Object.defineProperty(ArduinoLibraryProperties.prototype, key, {
    enumerable:false
  , configurable:true
  , get: function() {return this.getProperty(key)}
  })

  Object.defineProperty(ArduinoLibrary.prototype, key, {
    enumberable:false
  , configurable:true
  , get: function() {return this.properties.getProperty(key)}
  })
}

module.exports = ArduinoLibrary

module.exports.class = {
  ArduinoLibrary
, ArduinoLibraryProperties
}
