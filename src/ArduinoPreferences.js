const PropertyReader = require('properties-reader')

const fs = require('fs')
const path = require('@jamesarlow/tilde-path')
const _ = require('lodash')

/**
  An adapter for the preferences file which is in a .properties like format.
*/
class ArduinoPreferences {

  static get FILENAME () {return 'preferences.txt'}

  constructor(opts)
  {
    this._ide = opts.ide
    this._path = opts.path
  }

  get ide() {
    return this._ide
  }

  get path () {
    return (!!this._path && path.resolve(this._path))
        || path.resolve(this.ide.metaDir, ArduinoPreferences.FILENAME)
  }

  get exists() {
    return fs.existsSync(this.path)
  }

  read() {
    return this.exists ? ReadPreferences(this.path) : undefined
  }

  get(key) {
    return this.exists ? this.read().get(key) : undefined
  }
}

function ReadPreferences (file) {
  return PropertyReader(file)
}

module.exports = ArduinoPreferences
