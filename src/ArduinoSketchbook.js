const fs = require('fs')
const path = require('path')

const _ = require('lodash')

const ArduinoLibrary = require('./ArduinoLibrary')

// This is a circular dependency and must be dynamically required.
const ArduinoIDE = () => require('./ArduinoIDE')

class ArduinoSketchbook
{
  /**
    The key used to name the sketchbook path in `preferences.txt`
  */
  static get PREFERENCES_KEY () {
    return 'sketchbook.path'
  }

  /**
    The default sketchbook path for this os.
  */
  static get DEFAULT_PATH () {
    const platform = require('os').platform()

    /* Windows puts the data under documents. */
    if (/win32/.exec(platform))
      return '~/Documents/Arduino/'

    /* Linux puts the data under home. */
    if (/linux/.exec(platform))
      return '~/Arduino'
  }

  static get DEFAULT_PATH_RESOLVED () {
    return path.resolve(ArduinoSketchbook.DEFAULT_PATH)
  }

  /**
    Data property version of default constructor.
  */
  static get default () {
    return new ArduinoSketchbook ({
      path:ArduinoSketchbook.DEFAULT_PATH
    , ide:ArduinoIDE().default
    })
  }

  /**
  * @param opts
  * * `.ide` {ArduinoLibrary} To provide the path as a preferences value.
  * * `.path` {String} The path to the sketchbook root.
  */
  constructor(opts) {
    this._ide = opts.ide
    if (!!opts.path)
      this._path = opts.path
  }

  /**
  Deduces the path to the sketchbook root.

  Order of Resolution:

  1. The path set on this object (resolved to absolute).
  2. The preferences value from the ide set on this object.
  3. The default file path for the current os.

  @return {String} The path to the sketchbook root.
  */
  get path() {
    return (!!this._path && path.resolve(this._path))
        || (!!this.ide && this.ide.preferences.get(ArduinoSketchbook.PREFERENCES_KEY))
        || ArduinoSketchbook.DEFAULT_PATH_RESOLVED
  }

  get ide() {
    return this._ide
  }

  /**
   @return {boolean} If `this.path` exists and is a directory.
  */
  get exists() {
      return !this.path
          && fs.existsSync(this.path)
          && fs.statSync(this.path).isDirectory(this.path)
  }

  /**
  Gets the library path inside this sketchbook folder.

  For linux and windows, these are nested paths.

  For osx it should be the same as the base path.

  Default behavior is to return the base case.
  */
  get libraryPath() {
    const base = this.path

    const platform = require('os').platform()

    /* Windows nested libraries location */
    if (/win32/.exec(platform))
        return `${base}/Libraries/`

    /* Linux nested libraries */
    if (/linux/.exec(platform))
        return `${base}/libraries/`

    return base
  }

  /**
    Maps child directories to `ArudinoLibrary` instances and filters them
    for validity.

    @return {Array<ArduinoLibrary>}
      The list of valid libraries created from children directories.
  */
  get libraries() {
    if (this.exists)
    {
      return _.chain(fs.readdirSync(this.libraryPath))
       .map( p => path.resolve(this.libraryPath, p))
       .map( p => new ArduinoLibrary({path:p}))
       .filter( al => al.properties.exists)
       .value()
    }

    return []
  }
}

module.exports = ArduinoSketchbook
