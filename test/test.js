const src = '../src'

const ArduinoIDE = require(`${src}/ArduinoIDE`)
const ArduinoPreferences = require(`${src}/ArduinoPreferences`)
const ArduinoSketchbook = require(`${src}/ArduinoSketchbook`)

const assert = require('assert')

describe('class : ArduinoIDE', function() {

  it('Can locate the ide preferences metadata', function() {
    const prefs = ArduinoIDE.default.preferences
    const path = prefs.path
  })

  it('Can find the default sketchbook path (from preferences).', function() {
    const path = ArduinoIDE.default.sketchbook.path
  })

  it('Can find the possibly nested sketchbook library path.', function() {
    const path = ArduinoIDE.default.sketchbook.libraryPath

    assert(!!path, ArduinoIDE.default.sketchbook.exists)
  })

  it('Can filter the default sketchbook library path for valid library folder objects.', function() {
    const libs = ArduinoIDE.default.sketchbook.libraries
  })
})

describe('class : ArduinoPreferences : ', function() {

  describe('constructor : ', function() {
    it('handles `opts.ide`', function() {
      const prefs = new ArduinoPreferences({ide:ArduinoIDE.default})
      assert(!!prefs.ide)
    })

    it('handles `opts.path`', function() {
      const _path = '/home/preferences.txt'
      const prefs = new ArduinoPreferences({path:_path})
      assert.equal(prefs.path, _path)
    })
  })
})

describe('class : ArduinoSketchbook : ', function() {

  describe('static : ', function() {
    it ('has `DEFAULT_PATH` ', function() {
      assert(!!ArduinoSketchbook.DEFAULT_PATH
        , 'ArduinoSketchbook.DEFAULT_PATH should have a value.')

      assert(ArduinoSketchbook.DEFAULT_PATH.startsWith('~/')
        , 'The default path should be relative to the home directory')
    })

    it ('has `DEFAULT_PATH_RESOLVED` ', function() {
      assert(!!ArduinoSketchbook.DEFAULT_PATH_RESOLVED
        , 'ArduinoSketchbook.DEFAULT_PATH_RESOLVED should have a value.')

      assert(!ArduinoSketchbook.DEFAULT_PATH_RESOLVED.startsWith('~/')
        , 'The resolve path should no longer be relative.')

      const relative = ArduinoSketchbook.DEFAULT_PATH.substring(2)
      assert(ArduinoSketchbook.DEFAULT_PATH_RESOLVED.includes(relative)
        , 'The resolved path should include the relative part of the unresolved path.'
      )
    })

    it ('has `default` instance', function() {
      assert(ArduinoSketchbook.default instanceof ArduinoSketchbook
        , '`ArduinoSketchbook.default` should be an ArduinoSketchbook')

      assert.equal(ArduinoSketchbook.default.path, ArduinoSketchbook.DEFAULT_PATH_RESOLVED
        , '`ArduinoSketchbook.default.path` should be the resolved default path.')

      assert(!!ArduinoSketchbook.default.ide
        , '`ArduinoSketchbook.default.ide` should exist.')
    })
  })

  describe('constructor : ', function() {
    it ('handles `opts.ide` ', function() {
      const sketchbook = new ArduinoSketchbook({ide:ArduinoIDE.default})
      assert(!!sketchbook.ide, "{ide:...} `should set sketchbook.ide`")

      assert.equal(sketchbook.ide.metaDir, ArduinoIDE.default.metaDir)
    })

    it ('handles `opts.path` ', function() {
      const path = ArduinoSketchbook.DEFAULT_PATH
      const sketchbook = new ArduinoSketchbook({path:path})

      assert.equal(sketchbook._path, path
        , "The created sketchbook should store the explicit path.")

      assert.equal(sketchbook.path, ArduinoSketchbook.DEFAULT_PATH_RESOLVED
        , "The created sketchbook should infer the resolved path."
      )
    })
  })
})
